# ovcharov-ae-task

To create JAR file:

gradle fatJar 

Execution example:

java -jar ae-backend-xml-ovcharov-all-0.0.1.jar original_file_path diff_file_path


Outputs:

java -jar ae-backend-xml-ovcharov-all-0.0.1.jar ../../samples/sample-0-origin.html ../../samples/sample-1-evil-gemini.html
[INFO] 2019-05-31 01:30:45,096 c.a.o.d.b.f.ElementClassFilter - 6 - elements with class equal to 'btn' found
[INFO] 2019-05-31 01:30:45,098 c.a.o.d.b.f.ElementClassFilter - 2 - elements with class equal to 'btn-success' found
[INFO] 2019-05-31 01:30:45,100 c.a.o.d.b.f.ElementTitleFilter - 2 - elements with title equal to 'Make-Button' found
[INFO] 2019-05-31 01:30:45,101 c.a.o.d.b.f.ElementHrefFilter - 1 - elements with href equal to '#ok' found
[INFO] 2019-05-31 01:30:45,146 c.a.o.d.b.Application - Path to similar element  > html[0] > body[1] > div[0] > div[1] > div[2] > div[0] > div[0] > div[1] > a


java -jar ae-backend-xml-ovcharov-all-0.0.1.jar ../../samples/sample-0-origin.html ../../samples/sample-2-container-and-clone.html 
[INFO] 2019-05-31 01:30:25,283 c.a.o.d.b.f.ElementClassFilter - 5 - elements with class equal to 'btn' found
[INFO] 2019-05-31 01:30:25,285 c.a.o.d.b.f.ElementClassFilter - 0 - elements with class equal to 'btn-success' found
[INFO] 2019-05-31 01:30:25,286 c.a.o.d.b.f.ElementTitleFilter - 2 - elements with title equal to 'Make-Button' found
[INFO] 2019-05-31 01:30:25,287 c.a.o.d.b.f.ElementHrefFilter - 1 - elements with href equal to '#ok' found
[INFO] 2019-05-31 01:30:25,332 c.a.o.d.b.Application - Path to similar element  > html[0] > body[1] > div[0] > div[1] > div[2] > div[0] > div[0] > div[1] > div[0] > a


java -jar ae-backend-xml-ovcharov-all-0.0.1.jar ../../samples/sample-0-origin.html ../../samples/sample-3-the-escape.html 
[INFO] 2019-05-31 01:30:12,089 c.a.o.d.b.f.ElementClassFilter - 5 - elements with class equal to 'btn' found
[INFO] 2019-05-31 01:30:12,092 c.a.o.d.b.f.ElementClassFilter - 1 - elements with class equal to 'btn-success' found
[INFO] 2019-05-31 01:30:12,093 c.a.o.d.b.f.ElementTitleFilter - 0 - elements with title equal to 'Make-Button' found
[INFO] 2019-05-31 01:30:12,094 c.a.o.d.b.f.ElementHrefFilter - 2 - elements with href equal to '#ok' found
[INFO] 2019-05-31 01:30:12,148 c.a.o.d.b.Application - Path to similar element  > html[0] > body[1] > div[0] > div[1] > div[2] > div[0] > div[0] > div[2] > a



 java -jar ae-backend-xml-ovcharov-all-0.0.1.jar ../../samples/sample-0-origin.html ../../samples/sample-4-the-mash.html 
[INFO] 2019-05-31 01:29:21,292 c.a.o.d.b.f.ElementClassFilter - 5 - elements with class equal to 'btn' found
[INFO] 2019-05-31 01:29:21,294 c.a.o.d.b.f.ElementClassFilter - 2 - elements with class equal to 'btn-success' found
[INFO] 2019-05-31 01:29:21,295 c.a.o.d.b.f.ElementTitleFilter - 1 - elements with title equal to 'Make-Button' found
[INFO] 2019-05-31 01:29:21,296 c.a.o.d.b.f.ElementHrefFilter - 1 - elements with href equal to '#ok' found
[INFO] 2019-05-31 01:29:21,340 c.a.o.d.b.Application - Path to similar element  > html[0] > body[1] > div[0] > div[1] > div[2] > div[0] > div[0] > div[2] > a

