package com.agileengine.ovcharov.dmytro.backend;

import com.agileengine.ovcharov.dmytro.backend.comparator.ElementsComparator;
import com.agileengine.ovcharov.dmytro.backend.filters.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Application {

    private static Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private static String CHARSET_NAME = "utf8";

    private static String ORIGINAL_ELEMENT_ID = "make-everything-ok-button";

    private static Filter filterChain = new ElementHrefFilter(new ElementTitleFilter(new ElementClassFilter(new ElementIdFilter(null))));


    public static void main(String[] args) {

        if (args.length < 2) {
            LOGGER.error("Please set path for both files");
            System.exit(0);
        }

        String originalHtmlPath = args[0];
        String diffHtmlPath = args[1];

        findSimilarElement(new File(originalHtmlPath), new File(diffHtmlPath)).ifPresent(element ->
                {
                    LOGGER.info("Path to similar element {}",getPathToElement(element));
                }
        );

    }


    private static Optional<Element> findSimilarElement(File originalHtmlPath, File diffHtmlPath) {

        Optional<Document> originalDoc = parseDocument(originalHtmlPath);
        Optional<Document> diffDoc = parseDocument(diffHtmlPath);

        if (!originalDoc.isPresent() || !diffDoc.isPresent()) {
            return Optional.empty();
        }

        Optional<Element> originalElement = Optional.ofNullable(originalDoc.get().getElementById(ORIGINAL_ELEMENT_ID));


        if (originalElement.isPresent()) {
            List<Element> elements = filterAllSimilarElements(originalElement.get(), diffDoc.get());
            if (elements.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(elements.get(0));
            }

        }
        return Optional.empty();
    }

    private static List<Element> filterAllSimilarElements(Element originalElement, Document diffDoc) {
        Set<Element> similarElements = filterChain.filterSimilarElements(originalElement, diffDoc);
        List<Element> listOfElements = new ArrayList<>(similarElements);
        listOfElements.sort(new ElementsComparator(originalElement));

        return listOfElements;
    }

    private static Optional<Document> parseDocument(File htmlFile) {
        try {
            return Optional.of(Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath()));
        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    private static String getPathToElement(Element element) {
        List<String> pathToRoot = new LinkedList<>();
        pathToRoot.add(element.tagName());

        Element parent = element.parent();
        while (parent != null) {
            pathToRoot.add(parent.tagName() + "[" + parent.elementSiblingIndex() + "]");
            parent = parent.parent();
        }

        Collections.reverse(pathToRoot);
        pathToRoot.remove(0);
        StringBuilder path = new StringBuilder();
        for (String tag : pathToRoot) {
            path.append(" > ").append(tag);
        }
        return path.toString();
    }

}
