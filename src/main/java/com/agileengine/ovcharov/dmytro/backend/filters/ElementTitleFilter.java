package com.agileengine.ovcharov.dmytro.backend.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElementTitleFilter extends AbstractAttributeFilter {

    private static final String TITLE_ATTRIBUTE_KEY = "title";
    private static Logger LOGGER = LoggerFactory.getLogger(ElementTitleFilter.class);

    public ElementTitleFilter(AbstractFilter nextFilter) {
        super(nextFilter, TITLE_ATTRIBUTE_KEY, LOGGER);
    }

}
