package com.agileengine.ovcharov.dmytro.backend.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElementHrefFilter extends AbstractAttributeFilter {

    private static final String HREF_ATTRIBUTE_KEY = "href";
    private static Logger LOGGER = LoggerFactory.getLogger(ElementHrefFilter.class);

    public ElementHrefFilter(AbstractFilter nextFilter) {
        super(nextFilter, HREF_ATTRIBUTE_KEY, LOGGER);
    }

}
