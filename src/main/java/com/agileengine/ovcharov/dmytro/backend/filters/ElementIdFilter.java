package com.agileengine.ovcharov.dmytro.backend.filters;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class ElementIdFilter extends AbstractFilter {

    private static final String ID_ATTRIBUTE_KEY = "id";
    private static Logger LOGGER = LoggerFactory.getLogger(ElementIdFilter.class);

    public ElementIdFilter(AbstractFilter nextFilter) {
        super(nextFilter);
    }

    @Override
    public Set<Element> filterSimilarElements(Element originalElement, Document doc) {
        String id = originalElement.attr(ID_ATTRIBUTE_KEY);
        Set<Element> potentialElements = applyNextFilter(originalElement, doc);

        if (id.isEmpty()) {
            LOGGER.info("Original element doesn't have ID attr - skip ID filter");
            return potentialElements;
        }

        Element similarElement = doc.getElementById(id);

        if (similarElement != null) {
            LOGGER.info("Element with equal ID found - stop searching");
            HashSet<Element> elements = new HashSet<>();
            elements.add(similarElement);
            return elements;
        } else {
            return potentialElements;
        }
    }
}
