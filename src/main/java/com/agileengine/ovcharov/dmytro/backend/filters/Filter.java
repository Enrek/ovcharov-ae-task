package com.agileengine.ovcharov.dmytro.backend.filters;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Set;

public interface Filter {
    Set<Element> filterSimilarElements(Element originalElement, Document doc);
}
