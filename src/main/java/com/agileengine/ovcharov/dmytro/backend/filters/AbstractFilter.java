package com.agileengine.ovcharov.dmytro.backend.filters;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractFilter implements Filter {

    private AbstractFilter nextFilter;

    public AbstractFilter(AbstractFilter nextFilter) {
        this.nextFilter = nextFilter;
    }

    protected Set<Element> applyNextFilter(Element originalElement, Document doc) {
        if (nextFilter == null) {
            return new HashSet<>();
        } else {
            return nextFilter.filterSimilarElements(originalElement, doc);
        }
    }

    public abstract Set<Element> filterSimilarElements(Element originalElement, Document doc);

}
