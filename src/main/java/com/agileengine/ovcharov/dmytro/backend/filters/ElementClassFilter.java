package com.agileengine.ovcharov.dmytro.backend.filters;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class ElementClassFilter extends AbstractFilter {

    private static final String CLASS_ATTRIBUTE_KEY = "class";
    private static Logger LOGGER = LoggerFactory.getLogger(ElementClassFilter.class);

    public ElementClassFilter(AbstractFilter nextFilter) {
        super(nextFilter);
    }

    @Override
    public Set<Element> filterSimilarElements(Element originalElement, Document doc) {
        String[] classValues = originalElement.attr(CLASS_ATTRIBUTE_KEY).split(" ");
        Set<Element> potentialElements = applyNextFilter(originalElement, doc);

        if (classValues.length == 0) {
            LOGGER.info("Original element doesn't have {} attr - skip {} filter", CLASS_ATTRIBUTE_KEY);
            return potentialElements;
        }

        for (String classValue : classValues) {
            Elements similarElements = doc.select("." + classValue);
            LOGGER.info("{} - elements with class equal to '{}' found", similarElements.size(), classValue);
            potentialElements.addAll(similarElements);
        }

        return potentialElements;
    }
}
