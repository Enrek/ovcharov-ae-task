package com.agileengine.ovcharov.dmytro.backend.comparator;

import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ElementsComparator implements Comparator<Element> {

    private static final String CLASS_ATTRIBUTE_KEY = "class";
    private static final String HREF_ATTRIBUTE_KEY = "href";
    private static final String TITLE_ATTRIBUTE_KEY = "title";

    private Element originalElement;

    public ElementsComparator(Element originalElement) {
        this.originalElement = originalElement;
    }

    @Override
    public int compare(Element o1, Element o2) {
        Integer firstElementCoincidences = 0;
        Integer secondElementCoincidences = 0;

        String originalText = originalElement.text();
        String originalHref = originalElement.attr(HREF_ATTRIBUTE_KEY);
        String originalTitle = originalElement.attr(TITLE_ATTRIBUTE_KEY);
        List<String> originalClasses = Arrays.asList(originalElement.attr(CLASS_ATTRIBUTE_KEY).split(" "));

        if (isTextEqualsToOriginal(originalText, o1)) {
            firstElementCoincidences++;
        }

        if (isTextEqualsToOriginal(originalText, o2)) {
            secondElementCoincidences++;
        }

        if (isAttrEqualsToOriginal(HREF_ATTRIBUTE_KEY, originalHref, o1)) {
            firstElementCoincidences++;
        }

        if (isAttrEqualsToOriginal(HREF_ATTRIBUTE_KEY, originalHref, o2)) {
            secondElementCoincidences++;
        }

        if (isAttrEqualsToOriginal(TITLE_ATTRIBUTE_KEY, originalTitle, o1)) {
            firstElementCoincidences++;
        }

        if (isAttrEqualsToOriginal(TITLE_ATTRIBUTE_KEY, originalTitle, o2)) {
            secondElementCoincidences++;
        }

        firstElementCoincidences += (int) countCoincidentClasses(originalClasses, o1);
        secondElementCoincidences += (int) countCoincidentClasses(originalClasses, o2);


        return secondElementCoincidences - firstElementCoincidences;
    }


    private boolean isTextEqualsToOriginal(String originalText, Element element) {
        if (originalText.isEmpty())
            return false;
        return element.text().equals(originalText);
    }


    private boolean isAttrEqualsToOriginal(String attrKey, String originalValue, Element element) {
        if (originalValue.isEmpty())
            return false;
        return element.attr(attrKey).equals(originalValue);
    }

    private long countCoincidentClasses(List<String> originalClasses, Element element) {
        if (originalClasses.isEmpty())
            return 0;
        return Arrays.stream(element.attr(CLASS_ATTRIBUTE_KEY).split(" "))
                .filter(originalClasses::contains)
                .count();
    }

}
