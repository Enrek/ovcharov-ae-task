package com.agileengine.ovcharov.dmytro.backend.filters;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import java.util.Set;

public abstract class AbstractAttributeFilter extends AbstractFilter {

    private String attributeKey;
    private Logger logger;

    public AbstractAttributeFilter(AbstractFilter nextFilter, String attributeKey, Logger logger) {
        super(nextFilter);
        this.attributeKey = attributeKey;
        this.logger = logger;
    }

    public Set<Element> filterSimilarElements(Element originalElement, Document doc) {
        String titleValue = originalElement.attr(attributeKey);
        Set<Element> potentialElements = applyNextFilter(originalElement, doc);

        if (titleValue.isEmpty()) {
            logger.info("Original element doesn't have {} attr - skip {} filter", attributeKey);
            return potentialElements;
        }

        Elements similarElements = doc.select("[" + attributeKey + "*=" + titleValue + "]");
        logger.info("{} - elements with {} equal to '{}' found", similarElements.size(), attributeKey, titleValue);
        potentialElements.addAll(similarElements);

        return potentialElements;
    }

}
